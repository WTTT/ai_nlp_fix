from Predict import Predict
import numpy as np
from sklearn.metrics import accuracy_score
import json

model = Predict('MLP.joblib')

# received_data = str(input('Enter data: ')).split('|')

main_title = 'MSI GL65 Leopard 10SFK-062 15.6" FHD 144Hz 3ms Thin Bezel Gaming Laptop Intel Core i7-10750H RTX2070 16GB 512GB NVMe SSD Win 10'

# compare_titles = received_data[1]
# compare_titles = """New MSI P65 Creator 15.6'' 4K UHD Thin Laptop i7-9750H 16GB 512GB SSD RTX 2060
# ;Apple iPhone 11 Pro Max (64GB, Midnight Green) [Carrier Locked] + Carrier Subscription [Cricket Wireless]
# ;Apple iPhone 11 Pro Max Midnight Green 64GB Renwed
# ;Apple iPhone 11 Pro Max Midnight Green 64GB
# ;MSI GL65 Leopard 10SFK-062 15.6" FHD 144Hz 3ms Thin Bezel Gaming Laptop Intel...
# ;MSI GL65 Leopard 10SFK-062 15.6" FHD 144Hz 3ms Thin Bezel Gaming Laptop Intel Core i7-10750H RTX2070 16GB 512GB NVMe SSD Win 10
# ;MSI GL65 Leopard 10SFK-062 15.6" FHD 144Hz 3ms Thin Bezel Gaming Laptop Intel Core i7-10750H RTX2070 16GB 512GB NVMe SSD Win 10
# ;Tree Hut Tahitian Vanilla Bean Shea Sugar Scrub 18oz Ultra Hydrating and Exfo...
# ;Tree Hut Tahitian Vanilla Bean Shea Sugar Scrub, 18oz, Ultra Hydrating and Exfoliating Scrub for Nourishing Essential Body Care
# ;10 X Elite Milk Chocolate with Popping Candy (Pop Rocks) Kosher Israel Para Cow
# ;Happy Shole Society Vintage 1977 Funny T-Shirt Probably The Worst Year Ever 1977 Vintage Style - Unisex Humor T Shirt
# ;PS5 Slim 500gb
# ;ASUS Chromebook C300SA 13.3 Inch"""

compare_titles = """MSI GL65 Leopard 10SFK-062 15.6" FHD 144Hz 3ms Thin Bezel Gaming Laptop Intel...
;MSI GL65 Leopard 10SFK-062 15.6" FHD 144Hz 3ms  Intel Core i7-10750H RTX2070 16GB 512GB NVMe SSD Win 10
;Tree Hut Tahitian Vanilla Bean Shea Sugar Scrub 18oz Ultra Hydrating and Exfo...
;Outray Vintage Retro Classic Half Frame Horn Rimmed Clear Lens Glasses
;Sexy Anime Lewd Ahegao Hentai Waifu Girl Gag Gift
;HP GK1100 Gaming Gear Combo Keyboard Mouse Set Led Back Light Computers_amga
;BTS Official Lightstick Map of The Soul Special Edition (incl. One Random BTS Transparent Sticker)
;The Last of Us Part II -- Special Edition (Sony PlayStation 4, 2020)
;Chanel Python Ultimate Stitch Beige Quilted Shoulder Bag for Women
;Funko Pop Avengers Endgame: I Am Iron Man Glow-in-The-Dark Deluxe Vinyl Figure
;La Blanca Women's Wrap Underwire Push Up Bikini Swimsuit Top
;Vdealen Speed Cube Set, Includes 2x2 3x3 Stickerless Magic Cube Puzzle
;Apple AirPods with Wired Charging Case (2nd Generation)
;Singing Machine SMM-205 Unidirectional Dynamic Microphone with 10 Ft. Cord,Black
;Sandisk Ultra 128GB Micro SDXC UHS-I Card with Adapter -  100MB/s U1 A1 - SDS..."""


model.load_model()

model.load_input_from_json(main_title, compare_titles)

print(model.output_json())
