import MyLevenshtein

class StringProcess():
    '''Used to pre-process string'''    
    def __init__(self):
        self.nlp = MyLevenshtein.NLP_utils()
    
    def string_vectorizer(self, text1, text2):
        '''Using utils to calculate cosine similarity and characters similarity'''
        return [1, self.nlp.cosine_sim(text1, text2)]