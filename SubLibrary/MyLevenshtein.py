import nltk
import string
from sklearn.feature_extraction.text import TfidfVectorizer

class NLP_utils():

    def __init__(self):
        '''used to change plural form to singular form'''
        self.stemmer = nltk.stem.porter.PorterStemmer() 
        '''unnecceasry characters'''
        self.remove_punctuation_map = dict(
            (ord(char), None) for char in string.punctuation) 
        '''prpepare for tf-idf'''
        self.vectorizer = TfidfVectorizer(tokenizer=self.normalize, stop_words='english') 

    def stem_tokens(self, tokens):
        return [self.stemmer.stem(item) for item in tokens] 

    '''remove punctuation, lowercase, stem'''
    def normalize(self, text):
        return self.stem_tokens(nltk.word_tokenize(text.lower().translate(self.remove_punctuation_map)))

    def cosine_sim(self, text1, text2):
        tfidf = self.vectorizer.fit_transform([text1, text2])
        return ((tfidf * tfidf.T).A)[0, 1]

    def charaters_sim(self, text1, text2):

        '''Transform to list'''
        list_text1 = text1.lower().split()
        list_text2 = text2.lower().split()

        if(len(list_text1) >= len(list_text2)):
            maxNum = len(list_text2)
            simChar = 0
            for text in list_text2:
                if(text1.count(text) != 0):
                    simChar += 1
            return 2 * simChar / (maxNum)
        
        else:
            maxNum = len(list_text1)
            simChar = 0
            for text in list_text1:
                if(text2.count(text) != 0):
                    simChar += 1
            return 2 * simChar / (maxNum)
